(function($){

	var windowWidth = window.innerWidth || $(window).width();

	if(windowWidth >= 992) {
		console.log(windowWidth);
		var loading					= '.brk-sc-tiles-angle__loading',
				angleTop				= '.brk-sc-tiles-angle__top',
				angleBottom			= '.brk-sc-tiles-angle__bottom',
				$angle					= $('.brk-sc-tiles-angle'),
				$angleTopObj 		= $angle.find(angleTop),
				$angleBottomObj = $angle.find(angleBottom);

		$angleTopObj.hover(
				function() {
					$(this).siblings(angleBottom).addClass('no-active');
					$(this).siblings(loading).addClass('top-active');
				},
				function() {
					$(this).siblings(angleBottom).removeClass('no-active');
					$(this).siblings(loading).removeClass('top-active');
				}
		);

		$angleBottomObj.hover(
				function() {
					$(this).siblings(angleTop).addClass('no-active');
					$(this).siblings(loading).addClass('bottom-active');
				},
				function() {
					$(this).siblings(angleTop).removeClass('no-active');
					$(this).siblings(loading).removeClass('bottom-active');
				}
		);
	}


})(jQuery);