(function($) {
  Berserk.behaviors.sliders_page_init = {
    attach: function(context, settings) {
      var carouselContainer = $(".brk-carousel_project");
      var carousel = carouselContainer.find(".brk-carousel__items");
      var currentStep = carouselContainer.find(".brk-carousel__current");
      var stepsCount = carouselContainer.find(".brk-carousel__count");
      var prevBtn = carouselContainer.find(".brk-carousel__btn-prev");
      var nextBtn = carouselContainer.find(".brk-carousel__btn-next");

      prevBtn.click(function() {
        carousel.slick("slickPrev");
      });

      nextBtn.click(function() {
        carousel.slick("slickNext");
      });

      carousel.on("init", function(event, slick, currentSlide, nextSlide) {
        stepsCount.text(slick.slideCount);
        currentStep.text(slick.currentSlide + 1);
      });

      carousel.on("init afterChange", function(event, slick, currentSlide, nextSlide) {
        currentStep.text(slick.currentSlide + 1);
       
        if (slick.currentSlide === 0) {
          prevBtn.addClass("brk-carousel__btn-prev_disabled");
        } else prevBtn.removeClass("brk-carousel__btn-prev_disabled");

     
        if (slick.currentSlide + 1 === slick.slideCount) {
          nextBtn.addClass("brk-carousel__btn-next_disabled");
        } else nextBtn.removeClass("brk-carousel__btn-next_disabled");

      });

      carousel
        .slick({
          dots: false,
          prevArrow: false,
          nextArrow: false,
          infinite: false
        })
        .addClass("rendered");
    }
  };
})(jQuery);
