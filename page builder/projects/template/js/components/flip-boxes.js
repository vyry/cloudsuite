(function ($) {
  Berserk.behaviors.flip_boxes_init = {
    attach: function (context, settings) {

      function setSize(card) {
        var cardWidth = card.width(),
          backList = card.find('.flip-box__split-list'),
          backButtonsWidth = 0;

        card.find('.flip-box__split-actions a').each(function () {
          backButtonsWidth += $(this).width();
        });

        if (backButtonsWidth + 30 > cardWidth) {
          card.find('.flip-box__split-actions').addClass('flip-box__split-actions_small')
        }
        if (cardWidth < 300) {
          backList.addClass('flip-box__split-list_narrow')
        }
        var listMaxHeight = card.outerHeight() - card.find('.flip-box__split-title').outerHeight() - card.find('.flip-box__split-actions a').outerHeight() - 60;
        backList.css('height', listMaxHeight + 'px');
      }

      $('.flip-box__split').each(function(){
        setSize($(this));
      })

      $( window ).resize(function(){
        $('.flip-box__split').each(function(){
          setSize($(this));
        })
      })
    }
  }
})(jQuery);