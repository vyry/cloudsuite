(function ($) {
    Berserk.behaviors.blog__page_init = {
      attach: function (context, settings) {
        var carousel_container = $('.brk-related-carousel');
        var carousel = carousel_container.find('.brk-related-carousel__items');
      
        carousel.on('init afterChange', function (event, slick) {
          slick.$dots.removeClass('slick-dots')
          slick.$dots.addClass('brk-related-carousel__dots');
        });

        carousel
          .slick({
              dots: true,
              prevArrow: false,
              nextArrow: false,
              infinite: false,
              slidesToShow: 3,
              slidesToScroll: 1,
              responsive: [{
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }],
              })
            .addClass("rendered");

          }
      }
    })(jQuery);