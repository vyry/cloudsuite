(function ($) {
  Berserk.behaviors.portfolio_init = {
    attach: function (context, settings) {
      $('.brk-portfolio-card:not(.rendered)', context)
        .brk_hover3d('animation2',
          {
            imgWrapper: ".brk-portfolio-card__figure",
            caption: ".brk-portfolio-card__caption "
          }).addClass('rendered');
    }
  }
})(jQuery);
