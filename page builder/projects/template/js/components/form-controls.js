/**
 Berserk Forms Plugin
 */

(function ($) {

	var defaults = {
		formWrap: 'brk-form-wrap',
		formWrapActive: 'brk-form-wrap-active',
		inputLabel: 'input-label'
	};

	$.fn.berserkforms = function (params) {
		options = $.extend({}, defaults, params);

		var _ = this,
			i = 0;

		_.each(function () {
			var _ = $(this),
				textPlaceholder = null,
				imputId = null;

			if (_.attr('placeholder') != undefined) {
				textPlaceholder = _.attr('placeholder');
			} else {
				textPlaceholder = false;
			}
			_.removeAttr('placeholder');

			if (_.attr('id') != undefined) {
				imputId = _.attr('id');
			} else {
				imputId = 'brkin-' + i;
				_.attr('id', imputId);
			}

			_.wrap(function () {
				return '<div class="' + options.formWrap + '"></div>';
			});

			if (_.attr('readonly') == undefined) {
				if (textPlaceholder != false) {
					_.parent().append('<label class="' + options.inputLabel + '" for="' + imputId + '">' + textPlaceholder + '</label>');
				}
			}

			if (_.val() != '') {
				if (_.attr('readonly') == undefined) {
					_.parent().addClass(options.formWrapActive);
				}
			}

			_.focus(function () {
				if (_.attr('readonly') == undefined) {
					_.parent().addClass(options.formWrapActive);
				}
			});

			_.blur(function () {
				if (_.val() == '') {
					_.parent().removeClass(options.formWrapActive);
				}
			});

			i++;
		});

		return _;
	};

})(jQuery);


(function($) {

  Berserk.behaviors.form_controls = {
    attach: function (context, settings) {

  var brkForms = '.brk-form-strict [type="text"], .brk-form-strict [type="password"], .brk-form-strict [type="email"], .brk-form-strict [type="tel"], .brk-form-strict textarea';
	$(brkForms, context).berserkforms();

	if('select'){
    $('select', context).styler();
  }

	// Date
	(function () {
		var _ = $('[type="date"]'),
			wrap = 'brk-form-date-wrap';

  	_.each(function () {
  		var _ = $(this);
  			_.wrap(function () {
  				return '<div class="' + wrap + '"></div>';
  			});
  	});

  	_.parent().append('<span class="icon-before"><i class="fa fa-calendar-o" aria-hidden="true"></i></span>');

	})();


	// File
	$('.brk-form [type="file"]').each(function () {
		var _ = $(this),
			wrap = 'brk-form-file-wrap',
			wrapTr = 'brk-form-file-wrap-transparent';

		if (_.attr('type') == 'file') {
			if (_.parents().hasClass('brk-form-strict')) {
				_.wrap(function () {
					return '<label class="' + wrap + '"></label>';
				});
				_.parent().append('<span class="file-info">Choose file</span><span class="icon-before"><i class="fa fa-upload" aria-hidden="true"></i></span>');
			} else if (_.parents().hasClass('brk-form-round')) {
				_.wrap(function () {
					return '<label class="' + wrap + '"></label>';
				});
				_.parent().append('<span class="file-info">Choose file</span><span class="icon-before"><i class="fal fa-upload" aria-hidden="true"></i></span>');
			} else if (_.parents().hasClass('brk-form-transparent')) {
				_.wrap(function () {
					return '<label class="' + wrapTr + '"></label>';
				});
				_.parent().append('<span class="file-info">Choose file</span><span class="icon-before"><i class="fa fa-upload" aria-hidden="true"></i></span>');
			}

		}

		_.on('change', function () {
			var file = _.val(),
				fileInfo = $('.file-info');
			file = file.replace(/\\/g, "/").split('/').pop();
			fileInfo.html(file);
		});
	});

  // checkbox
  $('.brk-form [type="checkbox"]').each(function(){
		var _ = $(this),
			wrap = 'brk-form-checkbox';

		if (_.attr('type') == 'checkbox') {
  		_.wrap(function () {
  			return '<label class="' + wrap + '"></label>';
  		});

      _.parent().append('<span class="checkbox-custom"><i class="fa fa-check" aria-hidden="true"></i></span>');
		}
  });

  // radio
  $('.brk-form [type="radio"]').each(function(){
		var _ = $(this),
			wrap = 'brk-form-radio';

		if (_.attr('type') == 'radio') {
  		_.wrap(function () {
  			return '<label class="' + wrap + '"></label>';
  		});

      if(_.parents().hasClass('brk-form-transparent')) {
        _.parent().append('<span class="radio-custom"><i class="fa fa-check" aria-hidden="true"></i></span>');
      } else {
        _.parent().append('<span class="radio-custom"></span>');
      }

		}
  });


	/* --------------- Deleting placeholder focus --------------- */
	$('input,textarea').focus(function () {
		$(this).data('placeholder', $(this).attr('placeholder'));
		$(this).attr('placeholder', '');
	});
	$('input,textarea').blur(function () {
		$(this).attr('placeholder', $(this).data('placeholder'));
	});
	/* ------------- End Deleting placeholder focus ------------- */

    }
  }
})(jQuery);