// =================================================================================
// Dropdown effect
// =================================================================================

(function ($) {
  Berserk.behaviors.buttons_js = {
    attach: function (context, settings) {

    	var dropdownHeight;

    	jQuery('.btn__dropdown_wrap').each(function () {
    		if (!jQuery(this).hasClass('open')) {
    			jQuery(this).css({
    				'height': jQuery(this).children('li:first-child').innerHeight(),
    			});
    		} else {
    			dropdownHeight = 0;

    			jQuery(this).children().each(function () {
    				dropdownHeight += jQuery(this).outerHeight();
    			});

    			jQuery(this).css({
    				'height': dropdownHeight,
    			});
    		}
    	});

    	jQuery('.btn__dropdown_wrap').on('click', function (e) {
    		if (e.target !== this) {
    			return;
    		} else {
    			dropdownHeight = 0;

    			jQuery(this).children().each(function () {
    				dropdownHeight += jQuery(this).outerHeight();
    			});

    			jQuery(this).toggleClass('open');

    			if (!jQuery(this).hasClass('open')) {
    				jQuery(this).css({
    					'height': jQuery(this).children('li:first-child').innerHeight(),
    				});
    			} else {
    				jQuery(this).css({
    					'height': dropdownHeight,
    				});
    			}
    		}
    	});

  		$('.btn-pos')
  			.on('mouseenter', function (e) {
  				var parentOffset = $(this).offset(),
  					relX = e.pageX - parentOffset.left,
  					relY = e.pageY - parentOffset.top;
  				$(this).find('span').css({top: relY, left: relX})
  			})
  			.on('mouseout', function (e) {
  				var parentOffset = $(this).offset(),
  					relX = e.pageX - parentOffset.left,
  					relY = e.pageY - parentOffset.top;
  				$(this).find('span').css({top: relY, left: relX})
  			});



  		$('.slide-bg-wrap')
  			.on('mouseenter', function (e) {
  				var parentOffset = $(this).offset(),
  					relX = e.pageX - parentOffset.left,
  					relY = e.pageY - parentOffset.top;
  				$(this).find('.slide-bg').css({top: relY, left: relX})
  			})
  			.on('mouseout', function (e) {
  				var parentOffset = $(this).offset(),
  					relX = e.pageX - parentOffset.left,
  					relY = e.pageY - parentOffset.top;
  				$(this).find('.slide-bg').css({top: relY, left: relX})
  			});

        $('.btn-gradient')
          .on('mousemove',function(event){
            var parentOffset = $(this).offset();
            var x = event.pageX - parentOffset.left;
            var y = event.pageY - parentOffset.top;

            $(this).css({'--x': x, '--y': y});
          });

    }
  }
})(jQuery);