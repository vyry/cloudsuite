(function ($) {
    Berserk.behaviors.image_caption_init = {
        attach: function (context, settings) {
            $('.brk-tilter:not(.rendered)', context)
                .brk_hover3d('animation4',
                    {
                        imgWrapper: ".brk-tilter__figure",
                        lines: ".brk-tilter__deco--lines",
                        caption: ".brk-tilter__caption ",
                        overlay: ".brk-tilter__deco--overlay"
                    }).addClass('rendered');
        }
    }
})(jQuery);
