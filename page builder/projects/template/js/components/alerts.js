(function ($) {

  Berserk.behaviors.alerts_init = {
    attach: function (context, settings) {
      $window = $(window);
      var $alert = $('.alert', context);
      if ($alert.length) {
        var $self = {};
        $window.on('scroll', function () {
          $alert.each(function () {
            $self = $(this);
            $self = $(this);
            if ($self.isOnScreen()) {
              $self.addClass('show')
            }
          });
        }).trigger('scroll');
      }
    }
  }
})(jQuery);