(function ($) {
  Berserk.behaviors.shop_page_init = {
    attach: function (context, settings) {
      $('.brk-sc-item-page-tabs').each(function(){
        var container = $(this); 
        var width = container.find('.brk-sc-item-page-tabs__title').outerWidth();
        container.find('.brk-tabs-nav').css('min-width', width);
      })
    }
  }
})(jQuery); 
