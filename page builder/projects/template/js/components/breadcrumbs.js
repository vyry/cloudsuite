(function ($) {
  Berserk.behaviors.ranged_slider_init = {
    attach: function (context, settings) {
      $("#slider").slider({
        range: true,
        min: 1990,
        max: 2018,
        step: 1,
        values: [1996, 2011],
        slide: function (event, ui) {
          for (var i = 0; i < ui.values.length; ++i) {
            $("input.sliderValue[data-index=" + i + "]").val(ui.values[i]);
          }
        }
      });

      $("input.sliderValue").change(function () {
        var $this = $(this);
        $("#slider").slider("values", $this.data("index"), $this.val());
      });

      $('button#filter-trigger').click(function () {
        this.classList.toggle('closed');
        this.nextElementSibling.classList.toggle('closed');
      });

      $('button#categories-list-trigger').click(function () {
        var filtersContainer = this.parentNode.parentNode;
        filtersContainer.querySelector('#filter-trigger').classList.add('closed');
        filtersContainer.querySelector('.filter').classList.add('closed');
      });

    }
  }
})(jQuery);
