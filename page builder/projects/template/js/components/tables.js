(function($){

  function tableHvAct(el) {
    $(el).each(function() {
      var table   = $(this),
          active  = table.find('th.active'),
          iTh     = table.find('th').index(active);

      var td = table.find('td:nth-child('+(iTh + 1)+')'),
          th = table.find('th:nth-child('+(iTh + 1)+')');
      td.addClass('active');
      th.addClass('active');

      table.on('mouseover', 'td, th', function() {
        var tds     = $(this).parent().find('td'),
            ths     = $(this).parent().find('th'),
            indexTd = $.inArray( this, tds ),
            indexTh = $.inArray( this, ths ),
            ind     = indexTd + 1 || indexTh + 1;

        if(ind > 1) {
          var td = table.find('td:nth-child('+ind+')'),
              th = table.find('th:nth-child('+ind+')');
          td.addClass('hover');
          th.addClass('hover');
        }
      }).on('mouseout', 'td, th', function() {
        var tds     = $(this).parent().find('td'),
            ths     = $(this).parent().find('th'),
            indexTd = $.inArray( this, tds ),
            indexTh = $.inArray( this, ths ),
            ind     = indexTd + 1 || indexTh + 1;

        var td = table.find('td:nth-child('+ind+')'),
            th = table.find('th:nth-child('+ind+')');
        td.removeClass('hover');
        th.removeClass('hover');
      });
    });
  }

  tableHvAct('.brk-tables-trend');
  tableHvAct('.brk-tables-strict');

  /* brk-tables-lines__sort-nav */
  setTimeout(function () {
		$('.brk-tables').each(function () {
			var $this 			= $(this),
					$wrap 			= $this.find('.dataTables_info, .dataTables_paginate'),
					$paginate 	= $this.find('.dataTables_paginate'),
					$first			= $paginate.find('.first'),
					$previous		= $paginate.find('.previous'),
					$next				= $paginate.find('.next'),
					$last				= $paginate.find('.last');

			$wrap.wrapAll('<div class="brk-tables-lines__sort-nav"></div>');
			$first.prepend('<i class="fa fa-angle-double-left"></i>');
			$previous.prepend('<i class="fa fa-angle-left"></i>');
			$next.prepend('<i class="fa fa-angle-right"></i>');
			$last.prepend('<i class="fa fa-angle-double-right"></i>');
		});
	}, 400);
  /* brk-tables-lines__sort-nav */

	$('.brk-tables-strict table').DataTable();

	/* --------------- Deleting placeholder focus --------------- */
	$('input,textarea').focus(function () {
		$(this).data('placeholder', $(this).attr('placeholder'));
		$(this).attr('placeholder', '');
	});
	$('input,textarea').blur(function () {
		$(this).attr('placeholder', $(this).data('placeholder'));
	});
	/* ------------- End Deleting placeholder focus ------------- */

})(jQuery);