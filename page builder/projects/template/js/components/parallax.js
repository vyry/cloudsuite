(function ($) {
  Berserk.behaviors.parallax_init = {
    attach: function (context, settings) {
      var myParaxify = paraxify('.parallax-bg');

      var scroll_show = $(".scroll-show");
      if (scroll_show.length) {
        scroll_show.each(function () {
          var it = $(this);
          it.waypoint({
            handler: function () {
              it.addClass("in-view");
            },
            offset: "100%"
          })
        });
      }
    }
  }
})(jQuery);