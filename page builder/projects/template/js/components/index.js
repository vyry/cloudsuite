(function ($) {

  Berserk.behaviors.frontpage_init = {
    attach: function (context, settings) {

      window.lazySizesConfig = window.lazySizesConfig || {};
      lazySizesConfig.expand = 100;


      var lis = $('.brk-page-intro__sorting-item');
      var queue = $.Deferred().resolve(); // create an empty queue
      var speed = 170;
      lis.get().forEach(function (li) {
        queue = queue.then(function () {
          return $(li).animate({
            opacity: 1
          }, speed).promise();
        });
        speed = speed - 5;
      });


      var inp = $('.brk-page-intro__search--input'),
        arr = inp.data('placeholder').split(', '),
        i = 0,
        count = arr.length,
        int = 2000;

      function searchSubstitution() {
        inp.attr('placeholder', arr[i]);
        i++;

        if (i >= count) {
          i = 0;
        }
      }

      var intervalID = setInterval(searchSubstitution, int);

      /* --------------- Deleting placeholder focus --------------- */
      $('input,textarea').focus(function () {
        $(this).data('placeholder', $(this).attr('placeholder'));
        $(this).attr('placeholder', '');
        clearInterval(intervalID);
      });
      $('input,textarea').blur(function () {
        $(this).attr('placeholder', $(this).data('placeholder'));
        intervalID = setInterval(searchSubstitution, int);
      });
      /* ------------- End Deleting placeholder focus ------------- */


      $('.brk-page-intro').each(function (i) {
        var $this = $(this),
          $grid = $this.find('.brk-page-intro__items'),
          $buttonGroup = $this.find('.brk-page-intro__sorting'),
          $buttonItem = $this.find('.brk-page-intro__sorting-item'),
          $checked = $this.find('.checked'),
          $search = $this.find('.brk-page-intro__search'),
          $inputSearch = $search.find('.brk-page-intro__search--input');

        $buttonItem.each(function () {
          var $this = $(this),
            dataFilter = $this.data('filter'),
            $count = $this.find('.brk-page-intro__sorting-count');

          if (dataFilter !== '*') {
            $count.html($(dataFilter).length)
          }
        });

        // init Isotope
        window.addEventListener('load', function () {
          $grid.isotope({
            itemSelector: '.brk-page-intro__item',
            percentPosition: true,
            layoutMode: 'fitRows'
          });
        });

        // filter items on button click
        $buttonGroup.on('click', 'li', function () {
          var filterValue = $(this).data('filter');
          $grid.isotope({
            filter: filterValue
          });
        });


        /* autocomplete */
        var keywordsSortingArray = $inputSearch.data('keywords').split(', ');

        $inputSearch.autocomplete({
          source: keywordsSortingArray,
          select: function (event, ui) {
            $search.closest('form').submit(function (e) {
              e.preventDefault();
            });

            var val = ui.item.value;

            setTimeout(function () {
              if (val === '') {
                $grid.isotope({
                  filter: '*'
                });
              } else {
                $grid.isotope({
                  filter: '.filter-' + val
                });
              }
            }, 1000);

          }
        });
        /* End autocomplete */

        $inputSearch.on('change keyup input', function () {

          $search.closest('form').submit(function (e) {
            e.preventDefault();
          });

          var val = $(this).val();
          
          

          if (val === '') {
            setTimeout(function () {
              $grid.isotope({
                filter: '*'
              });
            }, 1000);
          } else {
            setTimeout(function () {
              $grid.isotope({
                filter: '.filter-' + val
              });
            }, 1000);
          }

        });


        $checked.trigger('click');

        $buttonGroup.each(function (i, buttonGroup) {
          var $buttonGroup = $(buttonGroup);
          $buttonGroup.on('click', 'li', function () {
            $buttonGroup.find(".checked").removeClass('checked');
            $(this).addClass('checked');
          });
        });
      });
    }
  }

})(jQuery);