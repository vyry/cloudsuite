(function ($) {

  Berserk.behaviors.accordions_init = {
    attach: function (context, settings) {


      $('.accordion').each(function () {
        var collapse = $(this).find('.collapse');

        collapse.each(function () {

          $(this).on('show.bs.collapse',function(){
            var curId = $(this).attr('id');
            collapse.each(function(){
              if($(this).attr('id') != curId){
                $(this).collapse('hide');  
              }
            })
          });

          $(this).on('shown.bs.collapse', function () {
            $(this).parents('.card').addClass('expanded');
          });

          $(this).on('hide.bs.collapse', function () {
            $(this).parents('.card').removeClass('expanded');
          });
        })
      })
    }
  }
})(jQuery);
