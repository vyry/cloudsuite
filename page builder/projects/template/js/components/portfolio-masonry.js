(function ($) {
  Berserk.behaviors.portfolio_masonry_init = {
    attach: function (context, settings) {

      var $grid = $('.brk-grid', context);
      if ($grid.length) {
        var $gridCols = $grid.attr('data-grid-cols');
        var $tabletGridCols,
          $gridWidth,
          $doubleGridWidth;
        var $currentWidth = $(window).width();

        if ($grid.attr('data-grid-cols-tablet')) {
          $tabletGridCols = $grid.attr('data-grid-cols-tablet')
        }

        function setCols(windowSize) {
          if (windowSize > 992) {
            $gridWidth = 100 / $gridCols + '%';
            $doubleGridWidth = 100 / $gridCols * 2 + '%';
            $grid.find('.brk-grid__sizer').css('width', $gridWidth);
            $grid.find('.brk-grid__item').css('width', $gridWidth);
            $grid.find('.brk-grid__item_width-2').css('width', $doubleGridWidth);
          } else if (windowSize <= 992 && $tabletGridCols) {
            $gridWidth = 100 / $tabletGridCols + '%';
            $doubleGridWidth = 100 / $tabletGridCols * 2 + '%';
            $grid.find('.brk-grid__sizer').css('width', $gridWidth);
            $grid.find('.brk-grid__item').css('width', $gridWidth);
            $grid.find('.brk-grid__item_width-2').css('width', $doubleGridWidth);
          } else if (windowSize <= 992 && !$tabletGridCols) {
            $gridWidth = 100 / $gridCols + '%';
            $doubleGridWidth = 100 / $gridCols * 2 + '%';
            $grid.find('.brk-grid__sizer').css('width', $gridWidth);
            $grid.find('.brk-grid__item').css('width', $gridWidth);
            $grid.find('.brk-grid__item_width-2').css('width', $doubleGridWidth);
          }
        }

        setCols($currentWidth);

        $(window).resize(function () {
          setCols($(window).width());
        });


        var iso = new Isotope($grid.get(0), {
          itemSelector: ".brk-grid__item",
          percentPosition: true,
          masonry: {
            columnWidth: ".brk-grid__sizer",
            rowHeight: ".brk-grid__sizer"
          },
          getSortData: {
            category_data: '[data-category]',
            name: '.brk-simple-card__title',
            category_name: '.brk-simple-card__category',
          }
        });

        window.addEventListener('load', function () {
          var iso = new Isotope($grid.get(0), {
            itemSelector: ".brk-grid__item",
            percentPosition: true,
            masonry: {
              columnWidth: ".brk-grid__sizer",
              rowHeight: ".brk-grid__sizer"
            },
            getSortData: {
              category_data: '[data-category]',
              name: '.brk-simple-card__title',
              category_name: '.brk-simple-card__category',
            }
          });
        });
      }

      // each filter count

      $('.brk-filters').find('.brk-filters__item').each(function () {
        var filterElem = $(this);
        var filterName = $(this).attr('data-filter');
        if (filterName && filterName != '*') { 
          var elemCount = $grid.find(filterName).length;
          filterElem.find('.brk-filters__count').html(elemCount);
        }
        if (filterName && filterName == '*') {
          var elemCount = $grid.find('.brk-grid__item').length;
          filterElem.find('.brk-filters__count').html(elemCount);
        }
      });


      $(".brk-filters").on("click", "li", function () {
        var filterValue = $(this).attr("data-filter");
        iso.arrange({
          filter: filterValue
        });
      });

      $(".brk-filters").each(function (i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on("click", "li", function () {
          $buttonGroup.find(".active").removeClass("active");
          $(this).addClass("active");
        });
      });

    }
  };
})(jQuery);
