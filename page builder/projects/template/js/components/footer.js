(function ($) {
  Berserk.behaviors.footer_init = {
    attach: function (context, settings) {

      // animated footer
      var footerWraps = $('.brk-footer__wrapper:not(.rendered)', context).addClass('rendered');
      footerWraps.each(function () {
        if ($(this).hasClass('brk-footer__wrapper_animated')) {

          var wrapper = $(this),
            footer = $(this).parent(),
            prevEl = footer.prev();

          footer.css('height', wrapper.outerHeight(true)).addClass('brk-footer_animated');
          prevEl.css('margin-bottom', wrapper.outerHeight(true)).css('background-color', "#fff").css('z-index', '2');

          $(window).scroll(function () {
            if (wrapper.outerHeight(true) !== footer.outerHeight(true)) {
              footer.css('height', wrapper.outerHeight(true)).addClass('brk-footer_animated');
              prevEl.css('margin-bottom', wrapper.outerHeight(true)).css('background-color', "#fff").css('z-index', '2');
            }
          })
        }
      })
      // animated footer end

      // Twitter init
      if($(".twitter-timeline").length){        
        window.twttr = (function (d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0],
            t = window.twttr || {};
          if (d.getElementById(id)) return t;
          js = d.createElement(s);
          js.id = id;
          js.src = "https://platform.twitter.com/widgets.js";
          fjs.parentNode.insertBefore(js, fjs);
  
          t._e = [];
          t.ready = function (f) {
            t._e.push(f);
          };
  
          return t;
        }(document, "script", "twitter-wjs"));
      }

      // Twitter init end


      $('.brk-styled-twitter').each(function(){
        var twitterEmbed = $(this);
        twitterEmbed.delegate('.twitter-timeline', 'DOMSubtreeModified propertychange', function () {
          var head = twitterEmbed.find('.twitter-timeline').contents().find('head');
          if (head){
            $('<link/>', {
              rel: "stylesheet",
              type: "text/css",
              href: "https://fonts.googleapis.com/css?family=Open+Sans:300,400"
            }).appendTo(head);
          }
          var singleTwits = twitterEmbed.find('.twitter-timeline').contents().find('.timeline-TweetList-tweet');
          singleTwits.each(function(){
            $(this).find('.timeline-Tweet-author').css("display", "none");
            $(this).find('.timeline-Viewport').css("overflow", "visible");
            $(this).find('.timeline-Tweet-inReplyTo').css("display", "none");
            $(this).find('.timeline-Tweet-text').css({
              marginLeft: "20px",
              fontSize: "18px",
              lineHeight: "30px",
              fontFamily: '"Open Sans", sans-serif',
              fontWeight: "300"

            });
            $(this).find('time').css("display", "none");
            $(this).find('.timeline-Tweet-media').css("display", "none");
            $(this).find('.timeline-Tweet-actions').css("display", "none");
            $(this).css("position", "relative");
            $(this).find('.timeline-Tweet-metadata').find('a').css({
              position: "absolute",
              left: "0",
              top: "0",
              width: "100%",
              height: "100%",
            })
            $(this).find('.timeline-Tweet-brand').css({
              position: "absolute",
              left: "-5px",
              top: "-8px",
              fontSize: "46px",
              zIndex: "-1",
              opacity: "0.2",
            }); 
          })
          if (twitterEmbed.hasClass('brk-styled-twitter_dark-color')){
            twitterEmbed.find('.twitter-timeline').contents().find('.timeline-Tweet-text').css({
              color: "#a2a5ad",
              marginLeft: "20px",
              fontSize: "18px",
              lineHeight: "30px",
              fontFamily: '"Open Sans", sans-serif',
              fontWeight: "300"
            });
          }
          if (twitterEmbed.hasClass('brk-styled-twitter_horizontal')) {
            twitterEmbed.find('.twitter-timeline').contents().find('.timeline-TweetList').css({
              display: "flex",
              flexWrap: "wrap"
            });
            twitterEmbed.find('.twitter-timeline').contents().find('.timeline-TweetList-tweet').css({
              width: "calc(100%/3)",
              position: "relative"
            });
          }
        });
      })

      $('.brk-styled-twitter-2').each(function(){
        var twitterEmbed = $(this);
        twitterEmbed.delegate('.twitter-timeline', 'DOMSubtreeModified propertychange', function () {
          var head = twitterEmbed.find('.twitter-timeline').contents().find('head');
          if (head){
            $('<link/>', {
              rel: "stylesheet",
              type: "text/css",
              href: "https://fonts.googleapis.com/css?family=Open+Sans:300,400"
            }).appendTo(head);

            $('<link/>', {
              rel: "stylesheet",
              type: "text/css",
              href: "https://fonts.googleapis.com/css?family=Montserrat:400,700"
            }).appendTo(head);

          }
          var singleTwits = twitterEmbed.find('.twitter-timeline').contents().find('.timeline-TweetList-tweet');
          singleTwits.each(function(){
            $(this).find('.TweetAuthor-avatar').css("display", "none");
            $(this).find('.timeline-Viewport').css("overflow", "visible");
            $(this).find('.timeline-Tweet-inReplyTo').css("display", "none");
            $(this).find('.timeline-Tweet-author').css("padding-left", "21px");
            $(this).find('.TweetAuthor-link').css("width", "100%");
            $(this).find('.TweetAuthor-name').css({
              fontFamily: '"Montserrat", sans-serif',
              fontSize: "16px",
              fontWeight: "700",
              color: "#ffffff",
            });
            $(this).find('.TweetAuthor-screenName').css({
              fontFamily: '"Montserrat", sans-serif',
              fontSize: "16px",
              fontWeight: "400",
              color: "#636566",
              float: "left",
              display: "inline-block"
                
            });
            $(this).find('.TweetAuthor-decoratedName').css({
              fontFamily: '"Montserrat", sans-serif',
              fontSize: "16px",
              fontWeight: "700",
              color: "#ffffff",
              display: "inline-block",
              float: "left"
            });
            $(this).find('.timeline-Tweet-text').css({
              marginLeft: "20px",
              fontSize: "14px",
              lineHeight: "21px",
              fontFamily: '"Open Sans", sans-serif',
              color: "#8d8e8f",
              marginLeft: "21px"

            });
            $(this).find('time').css("display", "none");
            $(this).find('.timeline-Tweet-media').css("display", "none");
            $(this).find('.timeline-Tweet-actions').css("display", "none");
            $(this).css("position", "relative");
            $(this).find('.timeline-Tweet-metadata').find('a').css({
              position: "absolute",
              left: "0",
              top: "0",
              width: "100%",
              height: "100%",
            })
            $(this).find('.timeline-Tweet-brand').css({
              position: "absolute",
              left: "0",
              top: "12px",
              fontSize: "13px"
            }); 
          })
        });
      })

      // Footer shop slider
      var footer_slider = $('.brk-slider_shop-footer');
      var nav_prev = footer_slider.find('.brk-slider__nav-prev');
      var nav_next = footer_slider.find('.brk-slider__nav-next');
      var items = footer_slider.find('.brk-slider__items');

      nav_prev.click(function () {
        items.slick("slickPrev");
      });

      nav_next.click(function () {
        items.slick("slickNext");
      });
      items.each(function(){
        $(this).slick({
          'accessibility': false,
          'arrows': false,
          'dots': false,
          'slidesToShow': 2,
          'slidesToScroll': 2,
          responsive: [{
            breakpoint: 850,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }, ]
        })
      })
      
      //footer shop slider end 
    }
  }
})(jQuery);
