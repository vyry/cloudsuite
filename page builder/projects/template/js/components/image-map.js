(function ($) {
  Berserk.behaviors.image_map_init = {
    attach: function (context, settings) {

      $('.brk-image-map').each(function() { 
        var $this 	= $(this),
            imData	= $this.data('brk-image-map');            
    
        setTimeout(function () {
          $this.imageMapPro(imData);
        }, 100);
      });

      var absoluteTopIM = function(){
        var absoluteTop       = $('.brk-image-map_absolute-top'),
            absoluteTopHeight = absoluteTop.height(),
            absoluteTopParent = absoluteTop.parent();

        absoluteTopParent.css('min-height', absoluteTopHeight);
      };

      window.addEventListener('load', absoluteTopIM);

      $(window).resize(function(){
        absoluteTopIM();
      });
    }
  }
})(jQuery); 
