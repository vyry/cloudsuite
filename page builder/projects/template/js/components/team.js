// =================================================================================
// Material Card
// =================================================================================
(function($){

  var $mcBtnAction = $('.mc-btn-action');
  if($mcBtnAction.length) {
    $mcBtnAction.click(function () {
      var card = $(this).parent('.brk-team-mc');
      var icon = $(this).children('i');
      icon.addClass('fa-spin-fast');

      if (card.hasClass('mc-active')) {
        card.removeClass('mc-active');
        setTimeout(function () {
          icon
            .removeClass('fa-arrow-left')
            .removeClass('fa-spin-fast')
            .addClass('fa-bars');
        }, 800);
      } else {
        card.addClass('mc-active');
        setTimeout(function () {
          icon
            .removeClass('fa-bars')
            .removeClass('fa-spin-fast')
            .addClass('fa-arrow-left');
        }, 800);
      }
    });
  }

})(jQuery);