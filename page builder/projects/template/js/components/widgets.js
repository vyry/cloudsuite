(function ($) {
  Berserk.behaviors.widgets_init = {
    attach: function (context, settings) {

      var sideBardCarousel = $('.brs-carousel:not(.rendered)', context).addClass('rendered');

      sideBardCarousel.on('init', function (event, slick) {
        slick.$dots.removeClass('slick-dots');
        slick.$dots.addClass('brs-carousel__dots-circled');
      });
 
      sideBardCarousel
        .slick({
          infinite: false,
          arrows: false,
          dots: true
        }).addClass('rendered');

      $('.brk-slider-cube:not(.rendered)', context).each(function () {
        $(this).addClass('rendered');
        var el = $(this),
          slider = el.children('.brk-slider-cube__items'),
          pagination = el.children('.brk-slider-cube__pagintaion'),
          overlayHorizontal = el.children('.brk-slider-cube__overlay');

        var cubeSlider = new Swiper(slider, {
          effect: 'cube',
          spaceBetween: 0,
          //simulateTouch: false,
          loop: true,
          loopedSlides: 2,
          autoHeight: true, //enable auto height
          speed: 900,
          cubeEffect: {
            shadow: false,
            slideShadows: false,
            shadowOffset: 20,
            shadowScale: 0.94
          },
          pagination: {
            el: pagination,
            clickable: true
          }
        });

        cubeSlider.on('touchStart', function () {
          if (!overlayHorizontal.hasClass('deactive')) {
            overlayHorizontal.addClass('deactive');
            setTimeout(function () {
              overlayHorizontal.removeClass('deactive');
            }, 500);
          } else {
            overlayHorizontal.removeClass('deactive');
          }
        });


        pagination.find('span').each(function () {
          $(this).on('click', function () {
            if (!overlayHorizontal.hasClass('deactive')) {
              overlayHorizontal.addClass('deactive');
              setTimeout(function () {
                overlayHorizontal.removeClass('deactive');
              }, 500);
            } else {
              overlayHorizontal.removeClass('deactive');
            }
          })
        })
      });

      var viewSwiper = $('.brk-sc-view-swiper:not(.rendered)').addClass('rendered');
      viewSwiper.find('.brk-sc-view-swiper__btn').each(function () {
        $(this).on('click', function () {
          if (viewSwiper.hasClass('row-view')) {
            viewSwiper.removeClass('row-view');
            viewSwiper.addClass('col-view');
          } else if (viewSwiper.hasClass('col-view')) {
            viewSwiper.removeClass('col-view');
            viewSwiper.addClass('row-view');
          }
        })
      });


      // Filter slider 
      var sliderContainers = $('.brk-sc-price-slider:not(.rendered)', context);

      sliderContainers.each(function () {
        var container = $(this);
        var slider = $(this).find('.brk-sc-price-slider__container');
        var input = $(this).find('input.sliderValue');
        var min_value = +slider.attr('data-min-value');
        var max_value = +slider.attr('data-max-value');

        container.addClass('rendered');
        slider.slider({
          range: true,
          min: min_value,
          max: max_value,
          step: 1,
          values: [min_value, max_value],
          slide: function (event, ui) {
            for (var i = 0; i < ui.values.length; ++i) {
              $("input.sliderValue[data-index=" + i + "]").val(ui.values[i]);
            }
          },
          change:function(){
            input.each(function(){
              setInputWidth($(this))
            })
          }
        });

        function setInputWidth(input){
          var strLegth = input.val().length;
          if (strLegth <= 1) {
            input.css('width', '14px')
          } else if (strLegth == 2) {
            input.css('width', '22px')
          } else if (strLegth == 3) {
            input.css('width', '30px')
          } else if (strLegth == 4) {
            input.css('width', '37px')
          } else if (strLegth >= 5) {
            input.css('width', '45px')
          }
        }
       
        input.each(function(){
          setInputWidth($(this))
        });
        input.on('keyup', function () {
          setInputWidth($(this))
        });
        
        input.change(function () {
          var $this = $(this);
          $(".brk-sc-price-slider__container").slider("values", $this.data("index"), $this.val());
        });
      })
      // Filter slider  end


    }
  }
})(jQuery);
