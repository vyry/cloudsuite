(function ($) {
  Berserk.behaviors.portfolio_isotope_init = {
    attach: function (context, settings) {

      var $grid = $(".brk-grid");
      var $gridCols = $grid.attr('data-grid-cols');
      var $tabletGridCols,
        $gridWidth,
        $doubleGridWidth;
      var $currentWidth = $(window).width();

      if ($grid.attr('data-grid-cols-tablet')) {
        $tabletGridCols = $grid.attr('data-grid-cols-tablet')
      }

      function setCols(windowSize) {
        if (windowSize > 992) {
          $gridWidth = 100 / $gridCols + '%';
          $doubleGridWidth = 100 / $gridCols * 2 + '%';
          $grid.find('.brk-grid__sizer').css('width', $gridWidth);
          $grid.find('.brk-grid__item').css('width', $gridWidth);
          $grid.find('.brk-grid__item_width-2').css('width', $doubleGridWidth);
        } else if (windowSize <= 992 && $tabletGridCols) {
          $gridWidth = 100 / $tabletGridCols + '%';
          $doubleGridWidth = 100 / $tabletGridCols * 2 + '%';
          $grid.find('.brk-grid__sizer').css('width', $gridWidth);
          $grid.find('.brk-grid__item').css('width', $gridWidth);
          $grid.find('.brk-grid__item_width-2').css('width', $doubleGridWidth);
        } else if (windowSize <= 992 && !$tabletGridCols) {
          $gridWidth = 100 / $gridCols + '%';
          $doubleGridWidth = 100 / $gridCols * 2 + '%';
          $grid.find('.brk-grid__sizer').css('width', $gridWidth);
          $grid.find('.brk-grid__item').css('width', $gridWidth);
          $grid.find('.brk-grid__item_width-2').css('width', $doubleGridWidth);
        }
      }

      setCols($currentWidth);

      $(window).resize(function () {
        setCols($(window).width());
      });

      var iso = new Isotope($grid.get(0), {
        itemSelector: ".brk-grid__item",
        percentPosition: true,
        masonry: {
          columnWidth: ".brk-grid__sizer",
          rowHeight: ".brk-grid__sizer"
        },
        getSortData: {
          category_data: '[data-category]',
          name: '.brk-simple-card__title',
          category_name: '.brk-simple-card__category',
        }
      });

      $grid.find('img').css('display','block');

      // each filter count
      
      $('.brk-filters').find('.brk-filters__item').each(function () {
        var filterElem = $(this);
        var filterName = $(this).attr('data-filter');
        if (filterName && filterName != '*') {
          var elemCount = $grid.find(filterName).length;
          filterElem.find('.brk-filters__count').html(elemCount);
        }
        if (filterName && filterName == '*') {
          var elemCount = $grid.find('.brk-grid__item').length;
          filterElem.find('.brk-filters__count').html(elemCount);
        }
      });


      $(".brk-filters").on("click", "li", function () {
        var filterValue = $(this).attr("data-filter").toLowerCase();
        iso.arrange({
          filter: filterValue
        });
      });

      $(".brk-filters").each(function (i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on("click", "li", function () {
          $buttonGroup.find(".active").removeClass("active");
          $(this).addClass("active");
        });
      });


      $('#brk-grid-sort').change(function () {
        var sortValue = $(this).val();
        iso.arrange({
          sortBy: sortValue
        });
        // Select sizer
        $("#brk-select__sizer-option").html($('#brk-grid-sort option:selected').text());
        $(this).width($("#brk-select__sizer").width() + 15);
      });


      var simpleCards = $(".brk-simple-card:not(.rendered)", context);
      if(simpleCards){
        simpleCards.each(function(){
          $(this).addClass("rendered");
          $(this).brk_hover3d("animation2", {
            imgWrapper: ".brk-simple-card__animation-wrapper",
            caption: ".brk-simple-card__info"
          });
        })
      }

      var swipeCards = $(".brk-btn-swipe-card:not(.rendered)", context);
      if(swipeCards){
        swipeCards.each(function(){
          $(this).addClass("rendered");
          $(this).brk_hover3d("animation2", {
            caption: ".brk-btn-swipe-card__caption"
          })
        })
      }

    }
  };
})(jQuery);
