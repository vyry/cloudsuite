(function ($) {
  'use strict';

  Berserk.behaviors.tabs_init = {
    attach: function (context, settings) {

      $('.brk-tabs:not(.rendered)', context).addClass('rendered').each(function () {
        if (!$(this).hasClass('brk-tabs-hovers')) {
          var _ = $(this),
            tab = _.find('.brk-tab'),
            tabItem = _.find('.brk-tab-item'),
            index = _.data('index'),
            hash = _.data('hash'),
            tabIndex = window.location.hash;

          index = index ? index : 0;

          setTimeout(function () {
            tabItem.hide().eq(index).fadeIn();
          }, 500);

          tab.on('click', function () {
            $(this).addClass('active').siblings('.active').removeClass('active no-hover');
            tabItem.hide().eq($(this).index()).fadeIn();
          }).eq(index).addClass("active");

        } else {

          var _ = $(this),
            tabsNav = _.find('.brk-tabs-nav'),
            tab = _.find('.brk-tab'),
            tabItem = _.find('.brk-tab-item'),
            index = _.data('index'),
            headerStyle_1 = _.closest('.brk-header_style-1'),
            headerStyle_2 = _.closest('.brk-header_style-2');

          index = index ? index : 0;

          if (headerStyle_1[0]) {
            _.addClass('brk-tabs-hovers_style-1');
          } else if (headerStyle_2[0]) {
            _.addClass('brk-tabs-hovers_style-2')
          }

          if (headerStyle_1[0]) {
            tabsNav.append('<div class="brk-tabs-hovers__duplicate-icon"></div>');
          }

          setTimeout(function () {
            tabItem.hide().eq(index).fadeIn();
            tab.eq(index).addClass('current');
          }, 500);

          if (headerStyle_1[0]) {
            var currentTab = _.find('.current'),
              icon = currentTab.children('[class*="fa-"]'),
              duplicateIcon = tabsNav.find('.brk-tabs-hovers__duplicate-icon');

            icon.clone().appendTo(duplicateIcon);
          }

          tab.on('mouseenter', function () {
            var $this = $(this),
              $icon = $this.children('[class*="fa-"]');

            if (headerStyle_1[0]) {
              duplicateIcon.empty();
              $icon.clone().appendTo(duplicateIcon);
            }

            if (!$this.hasClass('current')) {
              tab.removeClass('current').eq($(this).index()).addClass('current');
              tabItem.hide().eq($(this).index()).fadeIn();
            }
          });

        }
      });

      // magic-line
      var magicLine = '.brk-tabs-simple-top, .brk-tabs-simple-bottom, .brk-tabs-bottom-top, .brk-tabs-bottom-bottom, .brk-tabs-parallax, .brk-tabs_tabbed-filter';
      $(magicLine).each(function () {
        var _ = $(this),
          nav = _.find('.brk-tabs-nav'),
          tab = _.find('.brk-tab'),
          tabActive = _.find('.brk-tab.active');
        nav.append("<span class='magic-line'></span>");

        var magicLine = _.find(".magic-line");

        function magicLineFunc() {
          magicLine
            .width(tabActive.innerWidth())
            .css("left", tabActive.position().left)
            .data("origLeft", magicLine.position().left)
            .data("origWidth", magicLine.innerWidth());
        }

        setTimeout(function () {
          magicLineFunc();
        }, 1000);

        $(window).resize(function () {
          magicLineFunc();
        });

        tab.on('click', function () {
          setTimeout(function () {
            if (tab.hasClass('active')) {
              var tabActive = _.find('.brk-tab.active');
              magicLine
                .width(tabActive.innerWidth())
                .css("left", tabActive.position().left)
                .data("origLeft", magicLine.position().left)
                .data("origWidth", magicLine.innerWidth());
            }
          }, 100);
        });

        tab.hover(function () {
          var el = $(this);
          var leftPos = el.position().left;
          var newWidth = el.innerWidth();

          magicLine.stop().animate({
            left: leftPos,
            width: newWidth
          });

          var tabActive = _.find('.brk-tab.active');
          if (el.hasClass('active') == false) {
            tabActive.addClass('no-hover');
          }

        }, function () {
          magicLine.stop().animate({
            left: magicLine.data("origLeft"),
            width: magicLine.data("origWidth")
          });
          var tabActive = _.find('.brk-tab.active');
          tabActive.removeClass('no-hover');
        });

      });

      $('.brk-slider:not(.rendered)', context).each(function () {
        $(this).addClass('rendered');
        var container = $(this);
        var carousel = container.find('.brk-slider__items');
        var prev = $(this).find('.brk-slider__prev');
        var next = $(this).find('.brk-slider__next');

        carousel.on('init', function (event, slick, currentSlide, nextSlide) {
          var slider = $(this);
          prev.on('click', function () {
            slider.slick('slickPrev')
          });
          next.on('click', function () {
            slider.slick('slickNext')
          })
        });

        carousel.slick({
          appendDots: container.find('.brk-slider__control'),
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 2
              }
            }
          ]
        })
      })
    }
  }
})(jQuery);