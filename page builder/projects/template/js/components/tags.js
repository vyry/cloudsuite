(function ($) {
  setTimeout(function () {
    $('.brk-tags_bubble li a').each(function () {
      var elem = $(this),
        w = elem.innerWidth(),
        par = elem.parent();
      elem.css('height', w);
      par.css('width', w);
    });
  }, 1000);
})(jQuery);