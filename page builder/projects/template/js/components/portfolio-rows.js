(function ($) {
  Berserk.behaviors.portfolio_rows_init = {
    attach: function (context, settings) {

      var $grid = $(".brk-grid-fitrows");
      var $gridCols = $grid.attr('data-grid-cols');
      var $tabletGridCols,
        $gridWidth,
        $doubleGridWidth;
      var $currentWidth = $(window).width();
      var $gridHeight = $grid.attr('data-grid-height') + 'px';


      if ($grid.attr('data-grid-cols-tablet')) {
        $tabletGridCols = $grid.attr('data-grid-cols-tablet')
      }

      function setCols(windowSize) {
        if (windowSize > 992) {
          $gridWidth = 100 / $gridCols + '%';
          $doubleGridWidth = 100 / $gridCols * 2 + '%';
          $grid.find('.brk-grid-fitrows__item').css('width', $gridWidth);
          $grid.find('.brk-grid-fitrows__item_width-2').css('width', $doubleGridWidth);
        } else if (windowSize <= 992 && $tabletGridCols) {
          $gridWidth = 100 / $tabletGridCols + '%';
          $doubleGridWidth = 100 / $tabletGridCols * 2 + '%';
          $grid.find('.brk-grid-fitrows__item').css('width', $gridWidth);
          $grid.find('.brk-grid-fitrows__item_width-2').css('width', $doubleGridWidth);
        } else if (windowSize <= 992 && !$tabletGridCols) {
          $gridWidth = 100 / $gridCols + '%';
          $doubleGridWidth = 100 / $gridCols * 2 + '%';
          $grid.find('.brk-grid-fitrows__item').css('width', $gridWidth);
          $grid.find('.brk-grid-fitrows__item_width-2').css('width', $doubleGridWidth);
        }
      };
      setCols($currentWidth)

      $(window).resize(function () {
        setCols($(window).width());
      })


      $grid.find('.brk-grid-fitrows__item').css('height', $gridHeight);

      
      var iso = new Isotope($grid.get(0), {
      itemSelector: ".brk-grid-fitrows__item",
      percentPosition: true,
      layoutMode: 'fitRows',
      });

      

      $(".brk-simple-card-2:not(.rendered)", context)
        .brk_hover3d("animation2", {
          imgWrapper: ".brk-simple-card-2__animation-wrapper",
          caption: ".brk-simple-card-2__info"
        })
        .addClass("rendered");

    }
  };
})(jQuery);