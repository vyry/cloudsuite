(function ($) {

  Berserk.behaviors.slick_init = {
    attach: function (context, settings) {

      var _sl = {
        rs: $('div.rotation-slider', context),
        rs_slide: '.rotation-slider .slick-slide',
        rs_current: '.rotation-slider .slick-current'
      };

      function addPNClass(slide, current) {
        setTimeout(function() {
          $(slide).removeClass('prev-slid-index next-slid-index');
          $(current).prev().removeClass('next-slid-index').addClass('prev-slid-index');
          $(current).next().removeClass('prev-slid-index').addClass('next-slid-index');
        }, 100)
      }

      $(_sl.rs).each(function() {
        $(this).slick({
          dots: false,
          arrows: false,
          infinite: false,
          speed: 800,
          adaptiveHeight: true,
          focusOnSelect: true,
          centerMode: true,
          centerPadding: '75px',
          initialSlide: 1,
          swipeToSlide: true,
          responsive: [
            {
              breakpoint: 576,
              settings: {
                focusOnSelect: false,
                centerMode: false,
                centerPadding: '0'
              }
            },
          ]
        });
        // On before slide change
        addPNClass(_sl.rs_slide, _sl.rs_current);
        $(this).on('beforeChange', function(event, slick, currentSlide, nextSlide){
          addPNClass(_sl.rs_slide, _sl.rs_current);
        });
      });

      //
      // Skin .rotation-slider-min
      // Skin .slider-dark
      //
      $('div.rotation-slider-min', context).each(function() {
        $(this).slick({
          dots: false,
          arrows: false,
          infinite: true,
          speed: 800,
          swipeToSlide: true,
          adaptiveHeight: true
        });
      });

      $('div.slider-dark', context).each(function() {
        $(this).slick({
          dots: false,
          arrows: false,
          infinite: true,
          speed: 800,
          swipeToSlide: true,
          adaptiveHeight: true
        });
      });

      $('div.tiled-slider', context).each(function() {
        $(this).slick({
          dots: false,
          arrows: false,
          infinite: true,
          speed: 800,
          swipeToSlide: true,
          adaptiveHeight: true,
          swipe: false,
          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 2,
              }
            },
          ]
        });

      });

      $('div.triple-slider', context).each(function() {
        $(this).slick({
          slidesToShow: 3,
          dots: false,
          arrows: true,
          infinite: true,
          speed: 800,
          swipeToSlide: true,
          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
        })
      });

      $('div.slider-thumbnailed', context).each(function(i, elem) {
        $(elem).addClass('sthumb-' + i);
        var classElement = '.' + 'sthumb-' + i;

        $(classElement + ' .slider-thumbnailed-for').on('init', function(event, slick){
          $(classElement).removeClass('slick-loading');
        });

        $(classElement + ' .slider-thumbnailed-for').slick({
          dots: false,
          arrows: false,
          adaptiveHeight: true,
          asNavFor: classElement + ' .slider-thumbnailed-nav'
        });
        $(classElement + ' .slider-thumbnailed-nav').slick({
          asNavFor: classElement + ' .slider-thumbnailed-for',
          focusOnSelect: true,
          arrows: false,
          swipeToSlide: true,
          responsive: [
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 5
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 375,
              settings: {
                slidesToShow: 3
              }
            },
          ]
        });
      });


      $('div.slider-thumbnailed-full', context).each(function(i, elem) {
        $(elem).addClass('forNav-' + i);
        var classElement = '.' + 'forNav-' + i;

        $(classElement + ' .slider-thumbnailed-full-for').on('init', function(event, slick){
          $(classElement).removeClass('slick-loading');
        });

        $(classElement + ' .slider-thumbnailed-full-for', context).slick({
          dots: false,
          arrows: false,
          adaptiveHeight: true,
          prevArrow: '<button type="button" class="slick-prev">Prev</button>',
          nextArrow: '<button type="button" class="slick-next">Next</button>',
          asNavFor: classElement + ' .slider-thumbnailed-full-nav',
          responsive: [
            {
              breakpoint: 768,
              settings: {
                arrows: false
              }
            },
          ]
        });
        $(classElement + ' .slider-thumbnailed-full-nav', context).slick({
          asNavFor: classElement + ' .slider-thumbnailed-full-for',
          focusOnSelect: true,
          arrows: false,
          swipeToSlide: true,
          responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 5
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 4,
              }
            },
            {
              breakpoint: 375,
              settings: {
                slidesToShow: 3
              }
            },
          ]
        });
      });


      $('.default-slider, .post-angle-slider', context).each(function(i, elem) {
        $(this).slick({
          dots: false,
          arrows: false,
          infinite: true,
          speed: 800,
          swipeToSlide: true,
          adaptiveHeight: true
        });
      });


      $('.landscape-slider', context).each(function(i, elem) {
        $(elem).addClass('landscape-' + i);
        var classElement = '.' + 'landscape-' + i;

        setTimeout(function () {
          $(classElement + ' .landscape-slider-for').on('init', function(event, slick){
            $(classElement).removeClass('slick-loading');
          });

          $(classElement + ' .landscape-slider-for').slick({
            dots: false,
            arrows: false,
            adaptiveHeight: true,
            asNavFor: classElement + ' .landscape-slider-nav'
          });
          $(classElement + ' .landscape-slider-nav').slick({
            asNavFor: classElement + ' .landscape-slider-for',
            focusOnSelect: true,
            arrows: false,
          });
        }, 1000);

      });

      $('.brk-brand-slider', context).each(function(i,elem){

        $(this).on('init beforeChange afterChange', function (event, slick) {
          var maxHeight = 0;
   
          slick.$slideTrack.children().each(function () {
            if ($(this).find('img').height() > maxHeight) {
              maxHeight = $(this).height();
            }
          });
          slick.$slideTrack.children().each(function () {
            $(this).css("min-height", maxHeight)
          })
        });

        $(this).slick({
          slidesToShow: 6,
          slidesToScroll: 1,
          arrows: false,
          draggable: true,
          swipeToSlide: true,
          infinite: true,
          autoplay: true,
          autoplaySpeed: 3000,
          accessibility: false,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 3

              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2
              }
            }
          ]
        })

      });

      $('.brk-slider-team', context).each(function(){
        $(this).slick({
          slidesToShow: 8,
          slidesToScroll: 1,
          arrows: false,
          draggable: true,
          swipeToSlide: true,
          infinite: true,
          autoplay: true,
          autoplaySpeed: 5000,
          accessibility: false,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 5
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 3

              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2
              }
            }
          ]
        });
        
        var sliderItems = $(this).find('.brk-img-zoom');
          sliderItems.each(function (elem) {
            $(this).on('mouseenter', function () {
              var currentCard = $(this);
              sliderItems.each(function () {
                $(this).addClass('brk-img-zoom_not-hovered');
                currentCard.removeClass('brk-img-zoom_not-hovered');
              })
            });
          $(this).on('mouseleave', function () {
            sliderItems.each(function () {
              $(this).removeClass('brk-img-zoom_not-hovered');
            })
          })
        })

      });

      $('.brk-services-slider__items', context).each(function(){
        $(this).slick({
          "slidesToShow": 3,
          "slidesToScroll": 1,
          "swipeToSlide": true,
          "infinite": true,
          "accessibility": false,
          "autoplay": true,
          "autoplaySpeed": 3000,
          "arrows": false,
          "dots": true,
          "draggable": true,
          "pauseOnHover": true,
          "responsive": [{
            breakpoint: 1230,
            settings: {
              slidesToShow: 3
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1
            }
          },
          ]
        })
      });

      $('.brk-slider-shop', context).each(function () {

        var nav_prev = $(this).find('.brk-slider__nav-prev');
        var nav_next = $(this).find('.brk-slider__nav-next');
        var items = $(this).find('.brk-slider__items');

        nav_prev.click(function () {
          items.slick("slickPrev");
        });

        nav_next.click(function () {
          items.slick("slickNext");
        });

        items.slick({
          'accessibility': false,
          'arrows': false,
          'dots': false,
          'slidesToShow': 3,
          'slidesToScroll': 2,
          responsive: [{
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
          ]
        })

      });

      $('.brk-slider-cards', context).each(function(){

        var controlNext = $(this).find('.brk-slider__control-next');
        var controlPrev = $(this).find('.brk-slider__control-prev');
        var dots = $(this).find('.brk-slider__control');
        

        $(this).find('.brk-slider__items').slick({
          slidesToShow: 4,
          infinite: false,
          swipeToSlide: true,
          arrow: false,
          autoplay: true,
          autoplaySpeed: 3500,

          prevArrow: controlPrev,
          nextArrow: controlNext,
          appendDots: dots,
          dots: true,
          dotsClass: "brk-slider__dots",

          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1
              }
            }
          ]
        })
      });
    
      ///////////////////////////
      // Skin navigation panel //
      ///////////////////////////

      // element: .dots-base-skin
      setTimeout(function() {
        $('.dots-base-skin, .dots-landscape-skin', context).each(function(i, el) {
          var slickDots = $(el).find('.slick-dots');
          slickDots.wrap('<div class="dots-wrap"></div>');

          var dotsWrap = $(el).find('.dots-wrap');
          dotsWrap.prepend('<button class="l-prev" type="button"><i class="fas fa-angle-left"></i></button>');
          dotsWrap.append('<button class="l-next" type="button"><i class="fas fa-angle-right"></i></button>');

          var l_prev = dotsWrap.find('.l-prev');
          var l_next = dotsWrap.find('.l-next');
          l_prev.on('click', function(){$(el).slick("slickPrev")});
          l_next.on('click', function(){$(el).slick("slickNext")});
        });
      }, 100);

    }
  }

})(jQuery);