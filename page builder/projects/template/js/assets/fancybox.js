(function ($) {

  Berserk.behaviors.fancybox_init = {
    attach: function (context, settings) {

      var fancybox = $(".fancybox", context);
      if (fancybox.length) {
        fancybox.fancybox({
          openEffect: 'elastic',
          closeEffect: 'elastic'
        });
      }

      var fancybox_media = $('.fancybox-media', context);
      if (fancybox_media.length) {
        fancybox_media.fancybox({
          openEffect: 'fade',
          closeEffect: 'fade',
          helpers: {
            media: {}
          }
        });
      }

    }
  }
})(jQuery);
