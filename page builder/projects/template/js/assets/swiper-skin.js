(function ($) {

  Berserk.behaviors.swiper_slider_init = {
    attach: function (context, settings) {

      $('.filmstrip-slider', context).each(function () {
        var el = $(this),
          slider = el.find('.filmstrip-slider-container'),
          scrollbar = el.find('.brk-scrollbar'),
          scrollbarTrack = scrollbar.find('.brk-scrollbar-track'),
          scrollbarDrag = scrollbar.find('.brk-scrollbar-drag'),
          dataperwiew = el.data('perwiew'),
          dataSpaceBetween = el.data('spacebetween');


        dataperwiew = dataperwiew ? dataperwiew : 'auto';

        switch (true) {
          case el.attr('class').indexOf('timeline--strict') >= 0:
          case el.attr('class').indexOf('timeline--masonry') >= 0:
            var caption = '';
            var j = 0;

            if (typeof dataSpaceBetween == 'undefined' || dataSpaceBetween === '') {
              dataSpaceBetween = 76;
            } else {
              dataSpaceBetween = dataSpaceBetween;
            }

            var filmstrip = new Swiper(slider, {
              init: false,
              freeMode: false,
              slidesPerView: dataperwiew,
              resistance: true,
              spaceBetween: dataSpaceBetween,
              resistanceRatio: 0,
              // centeredSlides: true,
              // slidesPerGroup: 1,
              // initialSlide: 3,
              dynamicBullets: false,
              pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true,
                renderBullet: function (index, className) {
                  j++;
                  caption =  $(this.slides[index]).data("caption") ? "<strong>" + $(this.slides[index]).data("caption") + "</strong>" : "";
                  return '<span class="' + className + '" style="width: ' + (100 / (this.slides.length)) + '%;">' + caption + '</span>';
                }
              },
              on: {
                init: function () {
                  setActiveSlides(this.slides);
                },
                setTranslate: function () {
                  setActiveSlides(this.slides);
                },
                slideChangeTransitionEnd: function () {
                  setActiveSlides(this.slides);
                },
                slideChange: function () {
                  updateProgress();
                },
                resize: function () {
                  updateBulletsSize(this);
                }
              }
            });

            var $progressBar = $('<span class="swiper-progress__bar"></span>');

            setTimeout(function () {
              filmstrip.init();
              $progressBar.appendTo(filmstrip.pagination.$el);
            }, 200);

          function updateBulletsSize(filmstrip) {
            filmstrip.$el.find('.swiper-pagination > .swiper-pagination-bullet').each(function () {
              $(this).width(100 / j + '%');
            });
          }

            setTimeout(function () {
              updateBulletsSize(filmstrip);
            }, 200);

          function updateProgress() {
            filmstrip.pagination.bullets.each(function (i) {
              if ($(this).hasClass('swiper-pagination-bullet-active')) {
                $progressBar.css('transform', 'translate3d(' + ($(this).width() / 2 + $(this).offset().left - ($(window).width() / 2)) + 'px, 0px, 0px)');
              }
            });
          }

            setTimeout(function () {
              updateProgress();
            }, 200);

          function setActiveSlides(slides) {
            slides.each(function () {
              if ($(this).offset().left >= 0 && $(window).width() - ($(this).offset().left + $(this).width()) >= 0) {
                $(this).find('.timeline__item').addClass('active');
              } else {
                $(this).find('.timeline__item').removeClass('active');
              }
            });
          }

            $(window).on('resize', function () {
              updateBulletsSize(filmstrip);
            });
            break;

          case el.attr('class').indexOf('slider--scroll') >= 0:

            if (typeof dataSpaceBetween == 'undefined' || dataSpaceBetween === '') {
              dataSpaceBetween = 0;
            } else {
              dataSpaceBetween = dataSpaceBetween;
            }

            var filmstrip = new Swiper(slider, {
              init: false,
              freeMode: true,
              slidesPerView: dataperwiew,
              spaceBetween: dataSpaceBetween,
              resistance: false,
              resistanceRatio: 0,
              scrollbar: {
                el: scrollbar,
                draggable: true,
                snapOnRelease: false,
                dragSize: 8,
                dragClass: 'brk-scrollbar-drag'
              },
              on: {
                setTranslate: function () {
                  var progress = this.progress,
                    tranDuration = scrollbarDrag.css('transition-duration');

                  scrollbarTrack.css({'width': progress * 100 + '%'});
                }
              }
            });

            window.addEventListener('load', function () {
              filmstrip.init()
            });

            break;
        }

      });


      $('.staff-slider', context).each(function () {
        var el = $(this),
          slider = el.children('.staff-slider-container'),
          dotsSkin = el.children('.dots-base-staff-skin'),
          dataperwiew = el.data('perwiew'),
          pagination = dotsSkin.children('.pagination'),
          next = dotsSkin.children('.button-next'),
          prev = dotsSkin.children('.button-prev');

        dataperwiew = dataperwiew ? dataperwiew : 'auto';

        var staff = new Swiper(slider, {
          init: false,
          slidesPerView: dataperwiew,
          spaceBetween: 30,
          loop: true,
          loopFillGroupWithBlank: true,
          centeredSlides: true,
          slidesPerGroup: 3,
          pagination: {
            el: pagination,
            clickable: true,
            renderBullet: function (index, className) {
              return '<li class="' + className + '"></li>';
            },
          },
          navigation: {
            nextEl: next,
            prevEl: prev,
          },
          breakpoints: {
            480: {
              slidesPerGroup: 1,
              spaceBetween: 10
            },
            992: {
              slidesPerGroup: 1
            },
            1680: {
              slidesPerView: 'auto'
            }
          }
        });

        setTimeout(function () {
          staff.init()
        },800)
      });


      var colored = new Swiper('.colored-slider', {
        slidesPerView: 3,
        spaceBetween: 0,
        loop: true,
        loopedSlides: 4,
        speed: 1000,
        centeredSlides: true,
        loopFillGroupWithBlank: true,
        navigation: {
          nextEl: '.button-next',
          prevEl: '.button-prev',
        },
        breakpoints: {
          991: {
            slidesPerView: 2
          },
          767: {
            slidesPerView: 1
          }
        }
      });


      // 1 dash-one
      var dashOneSlider = new Swiper('.dash-one-slider', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        speed: 800,
        autoHeight: true, //enable auto height
        navigation: {
          nextEl: '.button-next',
          prevEl: '.button-prev',
        },
      });


      // 2 dash-two
      $('.brk-testimonials-dash-two', context).each(function () {
        var el = $(this),
          slider = el.children('.dash-two-slider'),
          pagination = el.children('.dash-two-pagination');

        var dashTwo = new Swiper(slider, {
          init: false,
          spaceBetween: 0,
          speed: 1000,
          initialSlide: 2,
          autoHeight: true, //enable auto height
          pagination: {
            el: pagination,
            bulletClass: 'dash-two-pagination-bullet',
            bulletActiveClass: 'dash-two-pagination-bullet-active',
            clickable: true,
            dynamicBullets: true
          },
        });

        setTimeout(function () {
          dashTwo.init();

          var objSlider = dashTwo.slides,
            childSlider = objSlider.children('.brk-testimonials-dash-two__text-reviews'),
            bullet = pagination.children('.dash-two-pagination-bullet'),
            i = 0;
          childSlider.each(function () {
            var _d = $(this).data('img'),
              _b = bullet[i];
            $(_b).css('backgroundImage', 'url(' + _d + ')');
            i++;
          });
        },300);
      });


      // 3 dash-three
      // var dashThreeSlider = new Swiper('.dash-three-slider', {
      //   slidesPerView: 3,
      //   spaceBetween: 20,
      //   centeredSlides: true,
      //   loopedSlides: 5,
      //   loop: true,
      //   speed: 800,
      //   pagination: {
      //     el: '.swiper-pagination-base',
      //     dynamicBullets: true,
      //     clickable: true
      //   },
      //   breakpoints: {
      //     992: {
      //       slidesPerView: 1,
      //       loopedSlides: 2,
      //       spaceBetween: 0,
      //     }
      //   }
      // });

      var dashThreeSlider;
      $('.dash-three-slider', context).each(function () {
        if ($(this).hasClass("dash-three-slider_single")) {
          dashThreeSlider = new Swiper('.dash-three-slider', {
            init: false,
            slidesPerView: 1,
            spaceBetween: 0,
            centeredSlides: true,
            loopedSlides: 5,
            loop: true,
            speed: 800,
            pagination: {
              el: '.swiper-pagination-base',
              dynamicBullets: true,
              clickable: true
            },
            breakpoints: {
              992: {
                slidesPerView: 1,
                loopedSlides: 2,
                spaceBetween: 0,
              }
            }
          });
        }
        else {
          dashThreeSlider = new Swiper('.dash-three-slider', {
            init: false,
            slidesPerView: 3,
            spaceBetween: 20,
            centeredSlides: true,
            loopedSlides: 5,
            loop: true,
            speed: 800,
            pagination: {
              el: '.swiper-pagination-base',
              dynamicBullets: true,
              clickable: true
            },
            breakpoints: {
              992: {
                slidesPerView: 1,
                loopedSlides: 2,
                spaceBetween: 0,
              }
            }
          });
        }

        setTimeout(function () {
          dashThreeSlider.init()
        }, 500)
      });


      // 4 dash-four
      $('.brk-testimonials-dash-four', context).each(function () {
        var el = $(this),
          slider = el.children('.dash-four-slider'),
          pagination = el.children('.dash-four-pagination');

        var dashFour = new Swiper(slider, {
          spaceBetween: 0,
          speed: 800,
          pagination: {
            el: pagination,
            bulletClass: 'dash-four-pagination-bullet',
            bulletActiveClass: 'dash-four-pagination-bullet-active',
            clickable: true
          },
        });
        var objSlider = dashFour.slides,
          childSlider = objSlider.children('.brk-testimonials-dash-four__item'),
          bullet = pagination.children('.dash-four-pagination-bullet'),
          i = 0;
        childSlider.each(function () {
          var _d = $(this).data('img'),
            _b = bullet[i];
          $(_b).css('backgroundImage', 'url(' + _d + ')');
          i++;
        });
      });


      // 5 dash-five
      var dashFive = new Swiper('.dash-five-slider', {
        slidesPerView: 3,
        spaceBetween: 30,
        loop: true,
        loopedSlides: 4,
        centeredSlides: true,
        speed: 1000,
        pagination: {
          el: '.pagination',
          clickable: true,
          renderBullet: function (index, className) {
            return '<li class="' + className + '"></li>';
          },
        },
        navigation: {
          nextEl: '.button-next',
          prevEl: '.button-prev',
        },
        breakpoints: {
          768: {
            slidesPerView: 1,
            loopedSlides: 2
          },
          992: {
            slidesPerView: 2,
            loopedSlides: 3
          }
        }
      });


      // 6 dash-six
      var dashSixSlider = new Swiper('.dash-six-slider', {
        slidesPerView: 3,
        spaceBetween: 0,
        //loop: true,
        speed: 800,
        navigation: {
          nextEl: '.dash-six-arrow-next',
          prevEl: '.dash-six-arrow-prev',
        },
        breakpoints: {
          768: {
            slidesPerView: 1,
          },
          992: {
            slidesPerView: 2,
          }
        }
      });


      // 02 double-slider
      $('.brk-testimonials-double__slider', context).each(function () {
        var el = $(this),
          slider = el.children('.double-slider'),
          pagination = el.children('.double-pagination');

        var doubleSlider = new Swiper(slider, {
          slidesPerView: 1,
          spaceBetween: 0,
          speed: 800,
          autoplay: {
            delay: 10000
          },
          pagination: {
            el: pagination,
            clickable: true,
            bulletClass: 'double-pagination-bullet',
            bulletActiveClass: 'double-pagination-bullet-active'
          }
        });
      });


      // 03 Layered Horizontal
      $('.brk-testimonials-layered-horizontal__container', context).each(function () {
        var el = $(this),
          slider = el.children('.layered-horizontal-slider'),
          sliderPrev = el.children('.button-prev'),
          sliderNext = el.children('.button-next'),
          overlayHorizontal = el.children('.overlay-horizontal');

        var layeredHorizontal = new Swiper(slider, {
          init:false,
          effect: 'cube',
          spaceBetween: 0,
          //simulateTouch: false,
          loop: true,
          loopedSlides: 2,
          autoHeight: true, //enable auto height
          speed: 900,
          cubeEffect: {
            shadow: false,
            slideShadows: false,
            shadowOffset: 20,
            shadowScale: 0.94
          },
          navigation: {
            nextEl: sliderNext,
            prevEl: sliderPrev
          }
        });

        setTimeout(function () {
          layeredHorizontal.init();

          layeredHorizontal.on('touchStart', function () {
            if (!overlayHorizontal.hasClass('deactive')) {
              overlayHorizontal.addClass('deactive');
              setTimeout(function () {
                overlayHorizontal.removeClass('deactive');
              }, 500);
            } else {
              overlayHorizontal.removeClass('deactive');
            }
          });
        },300);

        $(sliderPrev).on('click', function () {
          if (!overlayHorizontal.hasClass('deactive')) {
            overlayHorizontal.addClass('deactive');
            setTimeout(function () {
              overlayHorizontal.removeClass('deactive');
            }, 500);
          } else {
            overlayHorizontal.removeClass('deactive');
          }
        });

        $(sliderNext).on('click', function () {
          if (!overlayHorizontal.hasClass('deactive')) {
            overlayHorizontal.addClass('deactive');
            setTimeout(function () {
              overlayHorizontal.removeClass('deactive');
            }, 500);
          } else {
            overlayHorizontal.removeClass('deactive');
          }

        });
      });


      // 04 Layered Vertical
      $('.brk-testimonials-layered-vertical__container', context).each(function () {
        var el = $(this),
          slider = el.children('.layered-vertical-slider'),
          sliderPrev = el.children('.button-prev'),
          sliderNext = el.children('.button-next'),
          overlayHorizontal = el.children('.overlay-vertical'),
          layeredVerticalItem = '.layered-vertical-item';

        var layeredVertical = new Swiper(slider, {
          init:false,
          effect: 'flip',
          direction: 'vertical',
          spaceBetween: 0,
          autoHeight: true, //enable auto height
          //simulateTouch: false,
          loop: true,
          speed: 900,
          navigation: {
            nextEl: sliderNext,
            prevEl: sliderPrev
          }
        });

        setTimeout(function () {
          layeredVertical.init();

          layeredVertical.on('touchStart', function () {
            if (!overlayHorizontal.hasClass('deactive')) {
              overlayHorizontal.addClass('deactive');
              setTimeout(function () {
                overlayHorizontal.removeClass('deactive');
              }, 400);
            } else {
              overlayHorizontal.removeClass('deactive');
            }
          });
        },300);

        $(sliderPrev).on('click', function () {
          if (!overlayHorizontal.hasClass('deactive')) {
            overlayHorizontal.addClass('deactive');
            setTimeout(function () {
              overlayHorizontal.removeClass('deactive');
            }, 400);
          } else {
            overlayHorizontal.removeClass('deactive');
          }
        });

        $(sliderNext).on('click', function () {
          if (!overlayHorizontal.hasClass('deactive')) {
            overlayHorizontal.addClass('deactive');
            setTimeout(function () {
              overlayHorizontal.removeClass('deactive');
            }, 400);
          } else {
            overlayHorizontal.removeClass('deactive');
          }

        });
      });


      // 05 Circle
      $('.brk-testimonials-circle', context).each(function () {
        var el = $(this),
          slider = el.children('.circle-slider'),
          pagination = el.children('.circle-pagination');

        var circle = new Swiper(slider, {
          init: false,
          spaceBetween: 0,
          speed: 800,
          parallax: true,
          pagination: {
            el: pagination,
            bulletClass: 'circle-pagination-bullet',
            bulletActiveClass: 'circle-pagination-bullet-active',
            clickable: true
          }
        });

        setTimeout(function () {
          circle.init();

          var objSlider = circle.slides,
            childSlider = objSlider.children('.brk-testimonials-circle__item'),
            bullet = pagination.children('.circle-pagination-bullet'),
            i = 0;
          childSlider.each(function () {
            var _d = $(this).data('img'),
              _b = bullet[i];
            $(_b).css('backgroundImage', 'url(' + _d + ')');
            i++;
          });
        },300);

      });


      // brk-sc-row-three
      $('.brk-sc-row-three', context).each(function () {
        var el = $(this),
          slider = el.find('.swiper-container'),
          pagination = el.find('.brk-sc-row-three__pagination');

        var scRowThree = new Swiper(slider, {
          slidesPerView: 1,
          spaceBetween: 0,
          speed: 800,
          autoplay: {
            delay: 5000
          },
          pagination: {
            el: pagination,
            clickable: true,
            renderBullet: function (index, className) {
              return '<span class="' + className + '">0' + (index + 1) + '</span>';
            }
          }
        });
      });


      // brk-sc-row-four
      $('.brk-sc-row-four', context).each(function () {
        var el = $(this),
          slider = el.find('.swiper-container'),
          pagination = el.find('.brk-sc-row-four-pagination');

        var scRowThree = new Swiper(slider, {
          slidesPerView: 1,
          spaceBetween: 0,
          speed: 1200,
          autoHeight: true,
          autoplay: {
            delay: 5000
          },
          pagination: {
            el: pagination,
            clickable: true,
            bulletClass: 'brk-sc-row-four-pagination-bullet',
            bulletActiveClass: 'brk-sc-row-four-pagination-bullet-active'
          }
        });
      });


      // Default
      $('.brk-swiper-default', context).each(function () {
        var el = $(this),
          slider = el.find('.swiper-container'),
          sliderNext = el.find('.brk-swiper-default-nav-next'),
          sliderPrev = el.find('.brk-swiper-default-nav-prev'),
          pagination = el.find('.brk-swiper-default-pagination'),
          delay = el.data('delay'),
          param = {};

        if (delay) {
          param = {
            init: false,
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 1000,
            loop: true,
            autoHeight: true,
            autoplay: {
              delay: delay
            },
            navigation: {
              nextEl: sliderNext,
              prevEl: sliderPrev
            },
            pagination: {
              el: pagination,
              clickable: true,
              bulletClass: 'brk-swiper-default-pagination-bullet',
              bulletActiveClass: 'brk-swiper-default-pagination-bullet-active'
            }
          }
        } else {
          param = {
            init: false,
            slidesPerView: 1,
            spaceBetween: 0,
            speed: 1000,
            loop: true,
            autoHeight: true,
            navigation: {
              nextEl: sliderNext,
              prevEl: sliderPrev
            },
            pagination: {
              el: pagination,
              clickable: true,
              bulletClass: 'brk-swiper-default-pagination-bullet',
              bulletActiveClass: 'brk-swiper-default-pagination-bullet-active'
            }
          }
        }

        var brkDefaultSwiper = new Swiper(slider, param);

        window.addEventListener('load', function () {
          brkDefaultSwiper.init()
        })
      });

    }
  }
})(jQuery);