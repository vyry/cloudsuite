// Variables
// =========================================================================================
(function ($) {

var $html = $('html'),
	$document = $(document),
	$window = $(window),
	i = 0;

	$.fn.isOnScreen = function (test) {
		var height = this.outerHeight();
		var width = this.outerWidth();
		if (!width || !height) {
			return false;
		}

		var win = $(window);

		var viewport = {
			top: win.scrollTop(),
			left: win.scrollLeft()
		};
		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();

		var bounds = this.offset();
		bounds.right = bounds.left + width;
		bounds.bottom = bounds.top + height;

		var showing = {
			top: viewport.bottom - bounds.top,
			left: viewport.right - bounds.left,
			bottom: bounds.bottom - viewport.top,
			right: bounds.right - viewport.left
		};

		/*        if (showing.top > 0 && showing.left > 0 && showing.right > 0 && showing.bottom > 0){
		 $(this).trigger('inscreen');
		 }else{
		 $(this).trigger('outscreen');
		 }*/

		if (typeof test == 'function') {
			return test(showing);
		}

		return showing.top - height > 0
			&& showing.left > 0
			&& showing.right > 0
			&& showing.bottom - height > 0;
	};
	$.fn.isAppearOnScreen = function (test) {
		var height = this.outerHeight();
		var width = this.outerWidth();
		if (!width || !height) {
			return false;
		}

		var win = $(window);

		var viewport = {
			top: win.scrollTop(),
			left: win.scrollLeft()
		};
		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();

		var bounds = this.offset();
		bounds.right = bounds.left + width;
		bounds.bottom = bounds.top + height;

		var showing = {
			top: viewport.bottom - bounds.top,
			left: viewport.right - bounds.left,
			bottom: bounds.bottom - viewport.top,
			right: bounds.right - viewport.left
		};

		/*        if (showing.top > 0 && showing.left > 0 && showing.right > 0 && showing.bottom > 0){
		 $(this).trigger('inscreen');
		 }else{
		 $(this).trigger('outscreen');
		 }*/

		if (typeof test == 'function') {
			return test(showing);
		}

		return showing.top > 0
			&& showing.left > 0
			&& showing.right > 0
			&& showing.bottom > 0;
	};
	$.fn.rotate = function (degrees) {
		$(this).css({
			'-webkit-transform': 'rotate(' + degrees + 'deg)',
			'-moz-transform': 'rotate(' + degrees + 'deg)',
			'-ms-transform': 'rotate(' + degrees + 'deg)',
			'transform': 'rotate(' + degrees + 'deg)'
		});
		return $(this);
	};
	$.fn.setGradientBackground = function (color) {
		if (color.end) {
			$(this).css({
				'background': color.start,
				'background': '-moz-linear-gradient(top, ' + color.start + ' 0%, ' + color.end + ' 100%)',
				'background': '-webkit-gradient(left top, left bottom, color-stop(0%, ' + color.start + '), color-stop(100%, ' + color.end + '))',
				'background': '-webkit-linear-gradient(top, ' + color.start + ' 0%, ' + color.end + ' 100%)',
				'background': '-o-linear-gradient(top, ' + color.start + ' 0%, ' + color.end + ' 100%)',
				'background': '-ms-linear-gradient(top, ' + color.start + ' 0%, ' + color.end + ' 100%)',
				'background': 'linear-gradient(to bottom, ' + color.start + ' 0%, ' + color.end + ' 100%)',
				'filter': 'progid:DXImageTransform.Microsoft.gradient( startColorstr=' + color.start + ', endColorstr=' + color.end + ', GradientType=0 )'
			});
		} else {
			$(this).css({
				'background': color.start
			});
		}
		return $(this);
	};
	$.fn.addClassDelay = function (className, delay, condition) {
	condition = typeof condition != "undefined" ? condition : true;
	//console.log(condition);
	$(this).each(function (i) {
		if (condition) {
			$(this).delay(i * delay).queue(function (next) {
				$(this).addClass(className);
				next();
			});
		}
	});
};


// =================================================================================
// Icon wrap
// =================================================================================
var icon__wrap_main = $(".active__effect-main");
if (icon__wrap_main.length) {
    icon__wrap_main.each(function () {
        var icon__wrap = $(this).find(".active__effect");
        var icon__wrap_act = $(this).find(".active");
        icon__wrap.on("mouseenter", function () {
            icon__wrap.removeClass("current");
            icon__wrap.trigger("hover");
        });
        $(".active__effect-main").on("mouseleave", function () {
            icon__wrap.removeClass("current");
            icon__wrap_act.addClass("current");
        });
    });
}
})(jQuery);