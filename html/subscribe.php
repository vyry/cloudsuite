<?php
	// load in mailchimp library
	include('./MailChimp.php');
	
	// namespace defined in MailChimp.php
	use \DrewM\MailChimp\MailChimp;
	
	// connect to mailchimp
	$MailChimp = new MailChimp('e5998b13a557c052a7572a3bdcfa82aa-us19'); // put your API key here
	$list = '88958cee77'; // put your list ID here
  
	$email = $_GET['EMAIL']; // Get email address from form
	$id = md5(strtolower($email)); // Encrypt the email address
	$result = $MailChimp->put("lists/$list/members/$id", array(
  	'email_address'     => $email,
  	'status'            => 'subscribed',
  	'update_existing'   => true, // YES, update old subscribers!
  ));
	echo json_encode($result);